import { createRouter, createWebHistory } from 'vue-router'
import LPView from '../views/LaptopPrice.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'laptoppricepredict',
      component: LPView
    }
  ]
})

export default router
